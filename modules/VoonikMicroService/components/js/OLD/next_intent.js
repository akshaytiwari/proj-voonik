/**
 * Created by qison on 8/3/17.
 */
class NextIntentPrediction{
    run(intentData, sessionData){
        if(sessionData.name == "dressmaterial")
            return new Intent("dressmaterial", intentData);
        else if(sessionData.name == "dresses")
            return new Intent("dresses", intentData);
        else if(sessionData.name == "ethnic")
            return new Intent("ethnic", intentData);
        else if(sessionData.name == "kurtas")
            return new Intent("kurtas", intentData);
        else if(sessionData.name == "lehengas")
            return new Intent("lehengas", intentData);
        else if(sessionData.name == "sweaters")
            return new Intent("sweaters", intentData);
        else if(sessionData.name == "gowns")
            return new Intent("gowns", intentData);
        else if(sessionData.name == "jackets")
            return new Intent("jackets", intentData);
        else if(sessionData.name == "sarees")
            return new Intent("sarees", intentData);
        else if(sessionData.name == "shirts")
            return new Intent("shirts", intentData);
        else if(sessionData.name == "tunics")
            return new Intent("tunics", intentData);
    }
}